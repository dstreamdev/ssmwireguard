import io
import json
import subprocess
import os

import boto3
import click

WIREGUARD_PATH = "/bin/wg"

def build_config_string(host, users):
    conf = io.StringIO()
    conf.write("[Interface]\n")
    conf.write("PrivateKey = %s\n" % host["privatekey"])
    conf.write("ListenPort = %s\n\n" % host["listenport"])
    for user in users:
        conf.write("[Peer]\n")
        conf.write("# %s\n" % user["name"])
        conf.write("PublicKey = %s\n" % user["publickey"])
        conf.write("AllowedIPs = %s/32\n" % user["address"])
    return conf.getvalue()

@click.command()
@click.argument("interface")
@click.argument("config-path")
def update_config(interface, config_path):
    os.umask(0o177)
    ssm = boto3.client('ssm')
    response = ssm.get_parameter(Name='/wireguard/server', WithDecryption=True)
    host = json.loads(response["Parameter"]["Value"])
    paginator = ssm.get_paginator("get_parameters_by_path")
    responses = paginator.paginate(Path='/wireguard/users')
    users = []
    for response in responses:
        users += [{**{"name": r["Name"].split('/')[-1]}, **json.loads(r["Value"])} for r in response["Parameters"]]

    conf = build_config_string(host, users)

    try:
        with open(config_path, 'r') as f:
            old_conf = f.read()
    except FileNotFoundError:
        old_conf = None

    if not old_conf or old_conf != conf:
        print("Configuration changed, updating wireguard")
        with open(config_path, 'w') as f:
            f.write(conf)
        subprocess.run([WIREGUARD_PATH, "setconf", interface, config_path])

if __name__ == '__main__':
    update_config()
