Configures wireguard from json stored in AWS Parameter Store.

/wireguard/server - server configuration  
/wireguard/users/[name] - user configuration  
